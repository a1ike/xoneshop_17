$(document).ready(function () {

  $('.x-welcome').slick({
    arrows: true,
    dots: true,
    autoplay: true,
    speed: 500
  });

  $('.x-fresh__cards').slick({
    arrows: true,
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
  });

  $('.x-best__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
  });

  $('.x-news__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
  });

  $('.x-header__toggle').click(function () {
    $('.x-header__nav').slideToggle('fast', function () {
      // Callback
    });
  });

  $('.x-switch a').click(function (e) {
    e.preventDefault();
    $(this).parent().addClass('x-switch_current');
    $(this).parent().siblings().removeClass('x-switch_current');
    var tab = $(this).attr('href');
    $('.x-tab').not(tab).css('display', 'none');
    $(tab).fadeIn();
  });

  $('.x-collapse__header').click(function (e) {
    $(this).find('.x-collapse__arrow').toggleClass('x-collapse__arrow_rotated');
    $(this).next().slideToggle('fast');
  });

  $('.x-sidenav__droper > a').click(function () {
    $(this).next().slideToggle('fast');
  });

  $('a[href="#search"]').on('click', function (event) {
    event.preventDefault();
    $('#search').addClass('open');
    $('#search > form > input[type="search"]').focus();
  });

  $('#search, #search button.close').on('click keyup', function (event) {
    if (
      event.target == this ||
      event.target.className == 'close' ||
      event.keyCode == 27
    ) {
      $(this).removeClass('open');
    }
  });

  $('form').submit(function (event) {
    event.preventDefault();
    return false;
  });

});